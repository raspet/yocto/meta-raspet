#append file will add the ssh bootfile (to enable ssh by default) and a wpa-supplicant.conf file for wifi
#https://www.raspberrypi.org/documentation/configuration/boot_folder.md
#https://www.raspberrypi.org/documentation/configuration/wireless/headless.md
#https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

#Extends the search path the OpenEmbedded build system uses when looking for files and patches as it processes recipes and append files. The default directories BitBake uses when it processes recipes are initially defined by the FILESPATH variable. You can extend FILESPATH variable by using FILESEXTRAPATHS
#When extending FILESEXTRAPATHS, be sure to use the immediate expansion (:=) operator. Immediate expansion makes sure that BitBake evaluates THISDIR at the time the directive is encountered rather than at some later time when expansion might result in a directory that does not contain the files you need

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI_append = " file://wpa_supplicant.conf " 

do_deploy_append() {
    #When this file is present, SSH will be enabled on boot. The contents don't matter, it can be empty. SSH is otherwise disabled by default
    touch ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/ssh.txt

    cp ${WORKDIR}/wpa_supplicant.conf ${DEPLOYDIR}/${BOOTFILES_DIR_NAME}/
}
